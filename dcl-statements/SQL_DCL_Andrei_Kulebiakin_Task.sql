-- 1. Create a new user with the username "rentaluser" and the password "rentalpassword". 
-- 	Give the user the ability to connect to the database but no other permissions.
create user rentaluser with password 'rentalpassword';		-- alias for 'create role .. login'


-- 2. Grant "rentaluser" SELECT permission for the "customer" table. 
grant SELECT on customer to rentaluser;

--	Сheck to make sure this permission works correctly—write a SQL query to select all customers.
set role rentaluser;
	select * from customer;	
reset role;

-- 3. Create a new user group called "rental" and add "rentaluser" to the group.
create role rental;
grant rental to rentaluser;


-- 4. Grant the "rental" group INSERT and UPDATE permissions for the "rental" table. 
grant INSERT, UPDATE on rental to rental;

-- 	Insert a new row and update one existing row in the "rental" table under that role.
set role rental;
	insert into rental(rental_date, inventory_id, customer_id, return_date, staff_id, last_update)
		values(now(), 1, 1, now(), 1, now());
	update rental set customer_id = 2 where rental_id = 1;
reset role;


-- 5. Revoke the "rental" group's INSERT permission for the "rental" table. 
revoke INSERT on rental from rental;

-- 	Try to insert new rows into the "rental" table make sure this action is denied.
set role rental;
	insert into rental(rental_date, inventory_id, customer_id, return_date, staff_id, last_update)
	values(now(), 1, 1, now(), 1, now());
reset role;


-- 	6. Create a personalized role for any customer already existing in the dvd_rental database. 
create role client_AARON_SELBY;

-- 	The name of the role name must be client_{first_name}_{last_name} (omit curly brackets). 
-- 	The customer's payment and rental history must not be empty. 
select distinct c.customer_id, c.first_name, c.last_name from customer c
	join rental r on r.customer_id = c.customer_id
	join payment p on p.customer_id = r.customer_id
order by c.first_name asc;

-- 	Configure that role so that the customer can only access their own data in the "rental" and "payment" tables. 
alter table rental enable row level security;
alter table payment enable row level security;

create policy rental_row_policy on rental for select using (customer_id = 375);
create policy payment_row_policy on payment for select using (customer_id = 375);

grant select on rental, payment to client_AARON_SELBY;

-- 	Write a query to make sure this user sees only their own data.
set role client_AARON_SELBY;
	select * from rental;   -- should only return rentals for customer_id = 1
	select * from payment;  -- should only return payments for customer_id = 1
reset role;
