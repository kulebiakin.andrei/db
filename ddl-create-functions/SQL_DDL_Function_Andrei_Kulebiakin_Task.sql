-- 1. Create a view called "sales_revenue_by_category_qtr" 
create view sales_revenue_by_category_qtr as
select c.name, sum(p.amount) as total_sales_revenue 
from film f
	join film_category fc on fc.film_id = f.film_id
	join category c on c.category_id = fc.category_id
	join inventory i ON i.film_id = f.film_id
	join rental r ON r.inventory_id = i.inventory_id
	join payment p on p.rental_id = r.rental_id
where extract(QUARTER from r.rental_date) = extract(QUARTER from CURRENT_DATE)
	-- and extract(YEAR from r.rental_date) = extract(YEAR  from CURRENT_DATE) -- no data for the current year
group by c.name
having sum(p.amount) > 0;

-- Get data from view
select * from sales_revenue_by_category_qtr;

-- 2. Create a query language function called "get_sales_revenue_by_category_qtr"
-- Accepts 2 arguments: quarter number and year to represent current quarter
create function get_sales_revenue_by_category_qtr(current_quarter int, current_year int)
	returns table(name text, total_sales_revenue numeric)
	language SQL
as $$
	select c.name, sum(p.amount) as total_sales_revenue 
	from film f
		join film_category fc on fc.film_id = f.film_id
		join category c on c.category_id = fc.category_id
		join inventory i ON i.film_id = f.film_id
		join rental r ON r.inventory_id = i.inventory_id
		join payment p on p.rental_id = r.rental_id
	where extract(QUARTER from r.rental_date) = current_quarter
		-- and extract(YEAR from r.rental_date) = current_year -- no data for the current year
	group by c.name
	having sum(p.amount) > 0;
$$

-- Call function with arguments
select * from get_sales_revenue_by_category_qtr(2, 2024);

-- 3. Create a procedure language function called "new_movie"
create or replace function new_movie(in movie_title text)
	returns int
	language plpgsql
as $$
declare
	new_film_id int;
	language_name text := 'Klingon';
	film_language_id int;
begin
	raise notice 'Checking language availability for %', language_name;
	select language_id into film_language_id from language where name = language_name;
	if not found then
		raise exception 'language % not found', language_name;
	end if;

	raise notice 'Inserting new film with title %', movie_title;
	insert into film(
		title, 
		rental_rate, 
		rental_duration, 
		replacement_cost, 
		release_year, 
		language_id
		)
	values (
		movie_title, 
		4.99, 
		3, 
		19.99, 
		extract(YEAR from current_date), 
		film_language_id
		)
	returning film_id into new_film_id;

	raise notice 'New movie % added, ID = %', movie_title, new_film_id;

	return new_film_id;
end;
$$;

-- Call function to create new movie
select new_movie('Star Trek');